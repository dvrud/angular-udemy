import { Injectable } from "@angular/core";
import { User } from "../models/User";
import { Observable } from "rxjs";
import { of } from "rxjs";

@Injectable({
  providedIn: "root"
})
export class UserService {
  users: User[];
  data: Observable<any>;
  constructor() {
    this.users = [
      {
        firstname: "Vrunda",
        lastname: "Dharmik",
        email: "Dvv@dvd.com",
        isActive: true,
        registered: new Date("01/02/2018 08:30:00"),
        hide: true
      },
      {
        firstname: "Vrunda1",
        lastname: "Dharmik1",
        email: "Dd@vv.com",
        isActive: false,
        registered: new Date("01/03/2018 08:33:00"),
        hide: true
      },
      {
        firstname: "Vrunda2",
        lastname: "Dharmik2",
        email: "D@v.com",
        isActive: true,
        registered: new Date("01/04/2018 02:00:00"),
        hide: true
      }
    ];
  }

  // When working with an api always use the data as an observable like returned below.cz this is the async way of returning dat coming from an api.
  getUsers(): Observable<User[]> {
    return of(this.users);
  }

  addUser(user: User) {
    this.users.unshift(user);
  }
}
