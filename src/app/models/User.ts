export interface User {
  firstname: String;
  lastname: String;
  email: String;
  isActive?: boolean;
  registered?: any;
  hide?: boolean;
}
