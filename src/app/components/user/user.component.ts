import { Component, OnInit } from "@angular/core";
import { User } from "../../models/User";
@Component({
  selector: "app-user",
  templateUrl: "./user.component.html",
  styleUrls: ["./user.component.css"]
  // template: '<h2>VrundaD</h2>'
})
export class UserComponent implements OnInit {
  // Properties
  //   firstName = "Vrunda";
  //   lastName = "Dharmik";
  //   age = 21;
  //   address = {
  //     street: "vijaynagar",
  //     area: "Sangam",
  //     flat: "Nidhi"
  //   };
  // New properties with the interface.Thats why the above ones are commented.
  user: User;

  //   Constructor runs as soon as the class is used.So basically the constructor will run before the output is shown.
  constructor() {}

  // It is good practice to put all the below properties and methods into the ngonit method rather putting everything into the constructor.
  ngOnInit() {
    // this.sayHello();
    // console.log(this.age);
    // this.afterBirthday();
    // The new methods are below.
    this.user = {
      firstname: "Vrunda",
      lastname: "Dharmik",
      email: "dharmik@dharmik.com"
    };
  }
  //   sayHello() {
  //     console.log(`Hi ${this.firstName} ${this.lastName}`);
  //   }

  //   afterBirthday() {
  //     console.log((this.age += 1));

  // The new methods are below.
}
