import { Component, OnInit, ViewChild } from "@angular/core";
import { User } from "../../models/User";
import { UserService } from "../../services/user.service";
@Component({
  selector: "app-users",
  templateUrl: "./users.component.html",
  styleUrls: ["./users.component.css"]
})
export class UsersComponent implements OnInit {
  user: User = {
    firstname: "",
    lastname: "",
    email: ""
  };
  users: User[];
  loaded: boolean = false;
  showExtended: boolean = true;
  enableAdd: boolean = false;
  showUser: boolean = false;
  @ViewChild("userForm") form: any;

  constructor(private userService: UserService) {}

  ngOnInit() {
    this.userService.getUsers().subscribe(users => {
      this.users = users;
      this.loaded = true;
    });
  }
  // Method to add user dynamically.....the type is user interface in which the user object is defined.
  // adduser() {
  //   this.user.isActive = true;
  //   this.user.registered = new Date();
  //   this.users.unshift(this.user);

  //   this.user = {
  //     firstname: "",
  //     lastname: "",
  //     email: ""
  //   };
  // }

  onSubmit({ value, valid }: { value: User; valid: boolean }) {
    if (!valid) {
      console.log("The form submitted is not valid");
    } else {
      value.isActive = true;
      value.registered = new Date();
      value.hide = true;

      // this.users.unshift(value);
      this.userService.addUser(value);
      this.form.reset();
    }
  }
  // WE dotn need this toggle fumction as the function is fully pasted inside the click event.
  // toggleHide(user: User) {
  //   user.hide = !user.hide;
  // }
}
